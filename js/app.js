import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";


const firebaseConfig = {
  apiKey: "AIzaSyB9RW_gCpz8DlZXLXjrakH1AvoXMX3JLNE",
  authDomain: "administrador-web-bdcc0.firebaseapp.com",
  databaseURL: "https://administrador-web-bdcc0-default-rtdb.firebaseio.com",
  projectId: "administrador-web-bdcc0",
  storageBucket: "administrador-web-bdcc0.appspot.com",
  messagingSenderId: "646101017848",
  appId: "1:646101017848:web:b904014a2b5915f02a070d"
};

const app = initializeApp(firebaseConfig);
export { app }
export const auth = getAuth(app);
export const storage = getStorage(app);
export const database = getDatabase(app);

