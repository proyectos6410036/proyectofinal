import { database } from './app.js';
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';
import { getDatabase, ref, get } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';


const productosRef = ref(database, 'Productos');
const cardsContainer = document.querySelector('.cards-container');
const fragranceTypeSelect = document.getElementById('fragranceType'); // Obtén el select del HTML

async function fetchDataAndPopulateCards() {
    try {
        const snapshot = await get(productosRef);

        if (snapshot.exists()) {
            const productosData = snapshot.val();

            for (const key in productosData) {
                const productData = productosData[key];
                const cardDiv = createCardElement(productData);

                // Agrega el tipo de fragancia al select
                addFragranceTypeToSelect(productData.tipodefrag);

                cardsContainer.appendChild(cardDiv);
            }
        } else {
            console.log("The 'Productos' node does not contain data.");
        }
    } catch (error) {
        console.error('Error retrieving and populating data:', error);
    }
}

function createCardElement(data) {
    const cardDiv = document.createElement('div');
    cardDiv.classList.add('card');
    cardDiv.setAttribute('data-fragrance-type', data.tipodefrag);

    cardDiv.innerHTML = `
    <img src="${data.url}" class="card-img-top" alt="Product Image">
    <div class="card-body">
      <h5 class="card-title">${data.nombre}</h5>
      <p class="card-text">
        <strong>Cantidad:</strong> ${data.cantidad}<br>
        <strong>Descripción:</strong> ${data.descripcion}<br>
        <strong>Status:</strong> ${data.status}<br>
        <strong>Tipo de Fragancia:</strong> ${data.tipodefrag}<br>
      </p>
      <h4 class="card-text">$${data.precio}</h4>
      <button class="add-to-cart" data-id="${data.id}" data-price="${data.precio}">Agregar al carrito</button>
    </div>
  `;

    return cardDiv;
}

function addFragranceTypeToSelect(type) {
    if (fragranceTypeSelect.querySelector(`[value="${type}"]`) === null) {
        const option = document.createElement('option');
        option.value = type;
        option.textContent = type;
        fragranceTypeSelect.appendChild(option);
    }
}

window.addEventListener('load', fetchDataAndPopulateCards);

document.getElementById('searchButton').addEventListener('click', () => {
    const selectedFragranceType = fragranceTypeSelect.value;
    const productCards = document.querySelectorAll('.card');

    productCards.forEach((card) => {
        const cardType = card.getAttribute('data-fragrance-type');
        if (selectedFragranceType === 'todos' || selectedFragranceType === cardType) {
            card.style.display = 'block';
        } else {
            card.style.display = 'none';
        }
    });
});

const addToCartButtons = document.querySelectorAll('.add-to-cart');
const cartTotal = document.getElementById('cart-total');

let total = 0.00;

addToCartButtons.forEach(button => {
    button.addEventListener('click', () => {
        const productPrice = parseFloat(button.getAttribute('data-price'));
        total += productPrice;
        cartTotal.textContent = total.toFixed(2);
        alert('Producto agregado al carrito.');
    });
});

