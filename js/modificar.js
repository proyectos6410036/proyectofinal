import { database } from './app.js';
import { ref, get, set } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';

const productosRef = ref(database, 'Productos');
const productoIDSelect = document.getElementById('productoID');
const nuevaCantidadInput = document.getElementById('nuevaCantidad');
const nuevoStatusInput = document.getElementById('nuevoStatus');
const nuevoPrecioInput = document.getElementById('nuevoPrecio');

async function fetchProductNamesAndData() {
    try {
        const snapshot = await get(productosRef);

        if (snapshot.exists()) {
            const productosData = snapshot.val();

            for (const key in productosData) {
                const option = document.createElement('option');
                option.value = key;
                option.textContent = productosData[key].nombre; // Use the product name
                productoIDSelect.appendChild(option);
            }
        } else {
            console.log("El nodo 'Productos' no contiene datos.");
        }
    } catch (error) {
        console.error('Error al recuperar los nombres de los productos:', error);
    }
}

productoIDSelect.addEventListener('change', async (event) => {
    const productoID = event.target.value;
    if (productoID) {
        try {
            const snapshot = await get(ref(productosRef, productoID));

            if (snapshot.exists()) {
                const productoData = snapshot.val();
                nuevaCantidadInput.value = productoData.cantidad || '';
                nuevoStatusInput.value = productoData.status || '';
                nuevoPrecioInput.value = productoData.precio || '';
            } else {
                console.log("El producto seleccionado no contiene datos.");
            }
        } catch (error) {
            console.error('Error al recuperar los datos del producto:', error);
        }
    }
});

// Call the function to load product names when the page loads
window.addEventListener('load', fetchProductNamesAndData);

