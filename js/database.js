import { ref, push, getDatabase } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { app } from './app.js';

const dataForm = document.getElementById('dataForm');
const successMessage = document.getElementById('successMessage');

dataForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const Cantidad = dataForm.cantidad.value;
    const Descripcion = dataForm.descripcion.value;
    const Nombre = dataForm.nombre.value;
    const Status = dataForm.status.value;
    const TipodeFrag = dataForm.tipodefrag.value;
    const Url = dataForm.url.value;
    const Precio = dataForm.precio.value;

    const db = getDatabase(app);
    const dataRef = ref(db, 'Productos'); // Access the 'Productos' node in the database

    // Create a new object with the data entered
    const newData = {
        cantidad: Cantidad,
        descripcion: Descripcion,
        nombre: Nombre,
        status: Status,
        tipodefrag: TipodeFrag,
        url: Url,
        precio: Precio
    };

    // Add the new data to the 'Productos' node using push
    const newEntryRef = push(dataRef, newData);

    newEntryRef.then(() => {
        successMessage.innerText = 'Data added successfully to the database with ID: ' + newEntryRef.key;
        successMessage.style.display = 'block';
        // Clear the form
        dataForm.reset();
    }).catch((error) => {
        console.error('Error adding data to the database: ', error);
    });
});


