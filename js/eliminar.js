import { getDatabase, ref, remove, get } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { database } from './app.js';

const productoIDSelect = document.getElementById('productoID');

async function fetchProductIDs() {
    try {
        const snapshot = await get(ref(database, 'Productos'));

        if (snapshot.exists()) {
            const productosData = snapshot.val();

            // Clear existing options
            productoIDSelect.innerHTML = '';

            // Llena el elemento 'select' con las opciones de productos y sus nombres
            for (const productoID in productosData) {
                const producto = productosData[productoID];
                const option = document.createElement('option');
                option.value = productoID;
                option.textContent = `${productoID} - ${producto.nombre}`;
                productoIDSelect.appendChild(option);
            }
        } else {
            console.log("El nodo 'Productos' no contiene datos.");
        }
    } catch (error) {
        console.error('Error al recuperar los IDs de los productos:', error);
    }
}

// Llama a la función para cargar los IDs de productos cuando se carga la página
fetchProductIDs();

// Agregar un evento click al botón de "Eliminar Producto"
document.getElementById('eliminarButton').addEventListener('click', () => {
    // Aquí debes implementar la lógica para que el usuario seleccione el producto a eliminar.

    const selectedOptions = Array.from(productoIDSelect.selectedOptions);
    if (selectedOptions.length === 0) {
        console.error('Debes seleccionar al menos un producto a eliminar.');
        return;
    }

    const db = getDatabase();
    const deletePromises = [];

    // Elimina cada producto seleccionado
    selectedOptions.forEach((option) => {
        const productoID = option.value;
        const dataRef = ref(db, 'Productos/' + productoID);
        deletePromises.push(remove(dataRef));
    });

    Promise.all(deletePromises)
        .then(() => {
            // Después de eliminar los productos, vuelve a cargar los IDs actualizados
            fetchProductIDs();
            console.log('Productos eliminados con éxito.');
            // Lógica adicional después de eliminar los productos si es necesario
        })
        .catch((error) => {
            console.error('Error al eliminar los productos: ', error);
        });
});
