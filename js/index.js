import { ref, get, getDatabase } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { app } from './app.js';

const db = getDatabase(app);
const productosRef = ref(db, 'id');

get(productosRef)
  .then((snapshot) => {
    const productosContainer = document.getElementById('productos-container');

    if (snapshot.exists()) {
      const productosData = snapshot.val();

      for (let x = 0; x < Object.keys(productosData).length; x++) {
        const nodoProductoRef = ref(db, 'id/' + x);

        get(nodoProductoRef)
          .then((snapshot) => {
            const data = snapshot.val();

            // Crear un nuevo elemento card para el producto
            const card = document.createElement('div');
            card.classList.add('producto-card');

            // Crear la imagen del producto
            const imgElement = document.createElement('img');
            imgElement.src = data.url;

            // Crear el título del producto
            const title = document.createElement('div');
            title.classList.add('producto-title');
            title.textContent = data.nombre;

            // Crear el precio del producto
            const precio = document.createElement('div');
            precio.textContent = '$' + data.precio;

            // Agregar elementos al card
            card.appendChild(imgElement);
            card.appendChild(title);
            card.appendChild(precio);

            // Agregar el card al contenedor de productos
            productosContainer.appendChild(card);
          })
          .catch((error) => {
            console.error('Error al obtener datos del subnodo ' + x + ': ' + error);
          });
      }
    } else {
      console.log("El nodo 'Productos' no contiene datos.");
    }
  })
  .catch((error) => {
    console.error("Error al obtener datos de 'Productos':", error);
  });
  
